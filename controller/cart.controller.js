function renderCart(cartList) {
  contentHTML = ` <table class="table text-left">
  <thead>
    <tr>
      <th>Tên</th>
      <th>Giá</th>
      <th>Hình ảnh</th>
      <th>Số lượng</th>
      <th>Thành tiền</th>
    </tr>
  </thead>
<tbody>`;
  let totalPrice = 0;
  cartList.forEach((item) => {
    totalPrice += item.price * item.quantity;
    let rowHTML = `<tr>
              <td>${item.name}</td>
              <td>${item.price}</td>
              <td>    
                <img src=${item.img} class="img-product" />
              </td>
              
              <td>
                <button onclick=increaseQuantity(false,${
                  item.id
                }) class="btn btn-primary">-</button>
                <span class="mx-3">${item.quantity}</span>
                <button onclick=increaseQuantity(true,${
                  item.id
                }) class="btn btn-success">+</button>
              </td>
              <td>${item.price * item.quantity}</td>
              <td>
                <button onclick=deleteProduct(${item.id})
                  class="btn btn-danger"
                >
                  Xoá
                </button>
              </td>
             
            </tr>
            `;

    contentHTML += rowHTML;
  });

  // console.log("contentCart HTML before", contentHTML);
  contentHTML += `</tbody>
  </table> <p class="text-right font-weight-bold"><span id="totalPrice" class="mr-3">Tổng tiền:$ ${totalPrice}</span><button onclick=payout() class="font-weight-bold">Thanh toán</button></p>`;
  // console.log("contentCart HTML after", contentHTML);

  document.getElementById("product_list").innerHTML = contentHTML;
}
